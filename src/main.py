import os, sys, errmgr, hashlib

install = False
uninstall = False
search = False
update = False
kernel = False
force = False
pkgs = []
# Argument Parser
multlock = 0
index = 0
if len(sys.argv) - 1 == 0:
    print("ampm [ Optional Arguments ] { Required Arguments } { Package(s) }")
for arg in sys.argv:
    if index == 0:
        index += 1
        continue
    if arg == "install": 
        install = True
        multlock += 1
    elif arg == "uninstall":
        uninstall = True
        multlock += 1
    elif arg == "search":
        search = True
        multlock += 1
    elif arg == "update":
        update = True
        multlock += 1
    elif arg == "-K" or arg == "--kernel":
        kernel = True
    elif arg == "-f" or arg == "--force":
        force = True
    else:
        pkgs.append(arg)
        
if multlock > 1:
    errmgr.fatal("Unable to do multiple actions")

elif multlock < 1:
    errmgr.fatal("No Options selected")
# End of Argument Parser
if force:
    print("Force is currently not available, please run ampm again without force enabled")
    exit(1)

cachefl = open("ampm-cache", "r")
cache = cachefl.read()
if not os.getenv("HOME") + "/.local/bin" in os.getenv("PATH"):
    errmgr.warn(os.getenv("HOME") + "/.local/bin is not in $PATH, please add it to ~/.profile and reboot")

if install:
    finpackage = []
    for pkg in pkgs:
        #print(cache)
        if not pkg in cache and not force:
            
            errmgr.fatal("Package \"" + pkg + "\" was not found in cache, try updating the cache by running \"ampm update\"")
        
        # Cant get working correctly for some reason, once fixed we can get force re-enabled
        elif force and not pkg in cache: 
            errmgr.error("Package \"" + pkg + "\" was not found in cache, try updating the cache by running \"ampm update\"")
            print("Force mode enabled, skipping package")
            finpackage.append(pkg)
    confirm = input(str(len(pkgs)) + " Package(s) will be installed, do you with to continue?\n(Y/n): ")
    #urls = 
    if confirm.lower() == "y" or confirm == "":
        print("Starting download...")
        # Download
        for ln in cache.split("\n"):
            if not pkg in ln:
                continue
            url = ln.split("; ")[1]
            print(ln.split(":"[0]))
            if os.path.exists(ln.split(":")[0] + ".tar"):
                errmgr.warn("Cached file exists, using existing cached file")
            else:
                os.system("wget -q --show-progress \"" + url + "\" -O \"" + os.getenv("HOME") + "/.cache/ampm/dl/" + ln.split(":")[0] + ".tar\"")
            os.system("mkdir " + os.getenv("HOME") + "/.cache/ampm -p")
            os.system("tar xf \"" + ln.split(":")[0] + ".tar\" -C " + os.getenv("HOME") + "/.cache/ampm/" + ln.split(":")[0])
        # Verify via SHA256
        # on TODO
        for ln in cache.split("\n")
            if not pkg in ln:
                continue
            sha = ln.split("??? ")[1]
            shan = os.popen("sha256sum " + os.getenv("HOME") + "/.cache/ampm/dl/" + ln.split(":")[0])
            if sha == shan:
                print("SHA256 Verified")
            else:
                print("Error: SHA256 checksum is different, cached checksum: " + sha + " Downloaded checksum: " + shan + ", Deleting file...")
        
        # Install
        for dir in os.listdir(os.getenv("HOME") + "/.cache/ampm/"):
            os.system("cp usr/bin/* " + os.getenv("HOME") + "/.local/bin/")
            os.system("cp etc/* " + os.getenv("HOME") + "/.config")
            print("Finished install, cleaning cache...")
            os.system("rm " + os.getenv("HOME") + "/.cache/ampm/* -rf")
        print("Finished installing:")
        for pkg in pkgs:
            with pkginlist as open(os.getenv("HOME") + "/.local/share/ampm/pkginlist", "a"):
                pkginlist.write(pkg)
            print(pkgs)
        exit(0)
elif uninstall:
    inls = open(os.getenv("HOME") + "/.local/share/ampm/pkginlist").read()
    for pkg in pkgs:
        if not pkg in inls:
            errmgr.fatal("Package not found in installed list: " + pkg)