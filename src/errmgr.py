
def fatal(msg):
    print("Fatal Error: " + msg)
    exit(1)
def error(msg):
    print("Error: " + msg)
def warn(msg):
    print("Warning: " + msg)