
# THIS APP IS CURRENTLY IN DEVELOPMENT. DO NOT USE THIS FOR DAILY USE.
# Advanced Multispace Package Manager
Advanced Multispace Package Manager (or ampm) is a package manager that works both using user space and kernel space. Installing to user space allows you to install applications without needing a password.
### When to use User space
- Personal applications that only you would use, such as web browsers, entertainment apps (e.g Steam), etc. 
### When to use Kernel space
- When you want the application to be installed for all users
- To control the system such as settings, parental controls
- When the app would normally require previlidged access anyway like KVM, Partition Managers,
## Dependencies
- Linux 64 bit
- tar
- The latest version of Python 3
- wget or curl
## How to use ampm
ampm [[ Optional Arguments ]](###Optional) [{ Required Arguments }](###Required) { Package(s) }
### Required

```
install   (i)  -  Installs an application
uninstall (u)  -  Uninstalls an application
update
search    (s)  -  Searches downloaded cache for applications
instdist       -  Installs AMPM to a custom distro(ONLY TO BE USED BY DISTRO DEVELOPERS TO ADD AMPM TO THIER DISTRO)
```
### Optional

```
--color        (-c)  -  Enables color 
--verbose      (-v)  -  Enables Verbose mode
--kernel-space (-K)  -  Attempts to run command in kernel space mode
```
## Compilation
### Dependencies
check [Dependencies](##Dependencies) for dependencies
## Whats stable about the project:
- [ ] User space install
- [ ] User space uninstall
- [ ] User space update
- [ ] Kernel space install
- [ ] Kernel space uninstall
- [ ] Kernel space update
- [ ] instdist
- [ ] Search
- [ ] Color
## FAQ
Q: Can this replace my current package manager?

A: NO! do not replace this with your current package manager. This project is mainly supposed to be used either with newer distros, or only used in user space mode on multi-user systems.

Q: Why another package manager? especially with [Vanilla OS](https://vanillaos.org/)'s new package manager?

A:  AMPM is more designed to be used in user space, **kernel space should only be used on newer distros that want AMPM to be their main package manager**, not on a pre-existing distro such as [Debian](https://debian.org) or [Fedora](https://getfedora.org). And [Vanilla OS](https://vanillaos.org/)'s package manager, [apx](https://github.com/Vanilla-OS/apx), is  more similar to [flatpak](https://flatpak.org) or [snap](https://snapcraft.io/) with how it works, AMPM is more similar to normal package managers such as [apt](https://salsa.debian.org/apt-team/apt) and [dnf](https://github.com/rpm-software-management/dnf).

Q: How do I use AMPM in my own distro?

A: Please look below at [How to use distinst](##distinst) 

Q: Can I build this into a binary file?

A: Currently it is not possible to compile AMPM to a native binary. However, we are thinking of porting AMPM to C/C++.

## Distinst

### **This should only be read by distro developers**

After you create a distro, there are 3 ways to install AMPM on your distro

1. The instdist [Required Option](###Required) guided 

to use instdist guided method, run the following command:
```
ampm instdist guide /path/to/airoot/
```
and then answer the following questions, prepare for the following questions:

- Do you have a URL for AMPM to get packages from? if so, type the URL Here, if not leave it blank and continue
- What is your distros name?
- Will you have another system package manager installed(e.g apt, dnf, pacman)?
- If there is another system package manager, would you like to restrict kernel space mode?(this option will not show if you answer "no" to the previous question)

2. the instdist [Required Option](###Required) nonguided
to use the nonguided version to easily use this in a script, use this
```
ampm instdist nonguide name=put_dist_name_here syspacman=true(or false) restkern=true(or false, or none) /path/to/airoot/
```

3. the AMPM Install on Distro script on gitlab(currently unavailable) 

this method is similar to the manual install method above, except instead of ampm instdist nonguide, all you have to do is execute it
```
./ampm-instdist name=put_dist_name_here syspacman=true(or false) restkern=true(or false, or none) /path/to/airoot/
```